<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once('../../../config.php');
require_once($CFG->dirroot.'/grade/export/lib.php');
require_once('grade_export_wsexport.php');
require_once('vendor/autoload.php');

$id = required_param('id', PARAM_INT); // Course ID
$key = optional_param('key', '', PARAM_RAW);

$PAGE->set_url('/grade/export/wsexport/export.php', array('id' => $id));

if (!$course = $DB->get_record('course', array('id' => $id))) {
    print_error('nocourseid');
}

require_login($course);

$context = context_course::instance($id);

require_capability('moodle/grade:export', $context);
require_capability('gradeexport/wsexport:view', $context);

// We need to call this method here before any print otherwise the menu won't display.
// If you use this method without this check, will break the direct grade exporting (without publishing).
if (!empty($CFG->gradepublishing) && !empty($key)) {
    print_grade_page_head($COURSE->id, 'export', 'wsexport', get_string('wsexport:pagehead', 'gradeexport_wsexport'));
}

if (empty($USER->idnumber)) {
    print_error('wsexport:nouserid', 'gradeexport_wsexport');
}

if (empty($course->idnumber)) {
    print_error('wsexport:nocourseid', 'gradeexport_wsexport');
}

$groupid = groups_get_course_group($course, true);
if (groups_get_course_groupmode($course) == SEPARATEGROUPS &&
    !has_capability('moodle/site:accessallgroups', $context)) {
    if (!groups_is_member($groupid, $USER->id)) {
        print_error('cannotaccessgroup', 'grades');
    }
}

// The option 'idnumberrequired' excludes grade items that dont have an ID to use during import.
$formoptions = array(
    'idnumberrequired' => false,
    'includeseparator' => false,
    'multipledisplaytypes' => false,
    'publishing' => true,
    'simpleui' => true,
    'updategradesonly' => true
);
$mform = new grade_export_form(null, $formoptions);
$formdata = $mform->get_data();

$export = new grade_export_wsexport($course, $groupid, $formdata);

// If the gradepublishing is enabled and user key is selected print the grade publishing link.
if (!empty($CFG->gradepublishing) && !empty($key)) {
    groups_print_course_menu($course, 'index.php?id='.$id);
    echo $export->get_grade_publishing_url();
    echo $OUTPUT->footer();
} else {
    @header('Content-type: text/plain; charset=UTF-8');
    echo json_encode($export->print_grades());
    exit;

    // TODO
    $client = new GuzzleHttp\Client();
    @header('Content-type: text/plain; charset=UTF-8');
    try {
        $response = $client->post('https://sistemas.uel.br/pau/grades', array(
            'json' => $export->print_grades()
        ));
        echo $response->getStatusCode().PHP_EOL;
        echo $response->getReasonPhrase().PHP_EOL;
        echo $response->getBody().PHP_EOL;
    } catch (GuzzleHttp\Exception\ClientException $e) {
        echo $e->getRequest().PHP_EOL;
        echo $e->getResponse().PHP_EOL;
    }
}

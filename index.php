<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once('../../../config.php');
require_once($CFG->dirroot.'/grade/export/lib.php');
require_once('grade_export_wsexport.php');

$id = required_param('id', PARAM_INT); // Course ID

$PAGE->set_url('/grade/export/wsexport/index.php', array('id' => $id));

if (!$course = $DB->get_record('course', array('id' => $id))) {
    print_error('nocourseid');
}

require_login($course);

$context = context_course::instance($id);

require_capability('moodle/grade:export', $context);
require_capability('gradeexport/wsexport:view', $context);

print_grade_page_head($course->id, 'export', 'wsexport', get_string('wsexport:pagehead', 'gradeexport_wsexport'));

export_verify_grades($course->id);

if (!empty($CFG->gradepublishing)) {
    $CFG->gradepublishing = has_capability('gradeexport/wsexport:publish', $context);
}

if (empty($USER->idnumber)) {
    echo $OUTPUT->heading(get_string('wsexport:nouserid', 'gradeexport_wsexport'));
    echo $OUTPUT->footer();
    die;
}

if (empty($course->idnumber)) {
    echo $OUTPUT->heading(get_string('wsexport:nocourseid', 'gradeexport_wsexport'));
    echo $OUTPUT->footer();
    die;
}

if (groups_get_course_groupmode($course) == SEPARATEGROUPS && // Groups are being used
    !groups_get_course_group($course, true) &&
    !has_capability('moodle/site:accessallgroups', $context)) {
    echo $OUTPUT->heading(get_string('notingroup'));
    echo $OUTPUT->footer();
    die;
}

$actionurl = new moodle_url('/grade/export/wsexport/export.php');
// The option 'idnumberrequired' excludes grade items that dont have an ID to use during import.
$formoptions = array(
    'idnumberrequired' => false,
    'includeseparator' => false,
    'multipledisplaytypes' => false,
    'publishing' => true,
    'simpleui' => true,
    'updategradesonly' => true
);
$mform = new grade_export_form($actionurl, $formoptions);

groups_print_course_menu($course, 'index.php?id='.$id);
echo '<div class="clearer"></div>';

$mform->display();

echo $OUTPUT->footer();

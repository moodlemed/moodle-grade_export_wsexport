<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once($CFG->dirroot.'/grade/export/lib.php');

class grade_export_wsexport extends grade_export {

    public $plugin = 'wsexport';

    public $updatedgradesonly = false; // Default to export all grades

    /**
     * To be implemented by child classes
     * @param boolean $feedback
     * @return string
     */
    public function print_grades($feedback = false) {
        global $CFG, $USER;

        $exporttracking = $this->track_exports();

        $wsexport = array();
        $wsexport['teacherIdNumber'] = $USER->idnumber;
        $wsexport['courseIdNumber'] = $this->course->idnumber;
        $wsexport['students'] = array();

        $geub = new grade_export_update_buffer();
        $gui = new graded_users_iterator($this->course, $this->columns, $this->groupid);
        $gui->require_active_enrolment($this->onlyactive);
        $gui->allow_user_custom_fields($this->usercustomfields);
        $gui->init();
        while ($userdata = $gui->next_user()) {
            $user = $userdata->user;
            if (empty($user->idnumber)) {
                // ID number must exist otherwise we cant match up students when importing
                continue;
            }

            $wsexportstudent = array();
            $wsexportstudent['studentIdNumber'] = $user->idnumber;
            $wsexportstudent['grades'] = array();

            // studentgrades[] index should match with corresponding $index
            foreach ($userdata->grades as $itemid => $grade) {
                $gradeitem = $this->grade_items[$itemid];
                $grade->gradeitem =& $gradeitem;

                $wsexportgrade = array();

                // MDL-11669, skip exported grades or bad grades (if setting says so)
                if ($exporttracking) {
                    $status = $geub->track($grade);
                    if ($this->updatedgradesonly && ($status == 'nochange' || $status == 'unknown')) {
                        continue;
                    }
                    $wsexportgrade['state'] = $status;
                }

                // Only need ID number
                $wsexportgrade['assignment'] = $gradeitem->itemname;

                // Format and display the grade in the selected display type (real, letter, percentage)
                if (is_array($this->displaytype)) {
                    // Grades display type came from the return of export_bulk_export_data() on grade publishing
                    $wsexportgrade['score'] = array();
                    foreach ($this->displaytype as $gradedisplayconst) {
                        $gradestr = $this->format_grade($grade, $gradedisplayconst);
                        array_push($wsexportgrade['score'], $gradestr);
                    }
                } else {
                    // Grade display type submitted directly from the grade export form.
                    $gradestr = $this->format_grade($grade, $this->displaytype);
                    $wsexportgrade['score'] = $gradestr;
                }
                if ($this->export_feedback) {
                    $feedbackstr = $this->format_feedback($userdata->feedbacks[$itemid]);
                    $wsexportgrade['feedback'] = $feedbackstr;
                }
                array_push($wsexportstudent['grades'], $wsexportgrade);
            }
            array_push($wsexport['students'], $wsexportstudent);
        }
        $gui->close();
        $geub->close();
        return $wsexport;
    }

}

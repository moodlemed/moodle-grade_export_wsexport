<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['pluginname'] = 'WSExport';
$string['wsexport:publish'] = 'Exportar livro de notas com o WSExport';
$string['wsexport:view'] = 'Usar a exportação do livro de notas com o WSExport.';
$string['wsexport:pagehead'] = 'Exportar livro de notas com o WSExport';
$string['wsexport:nouserid'] = 'Você não pode usar a exportação do livro de notas com o WSExport.';
$string['wsexport:nocourseid'] = 'O livro de notas deste curso não pode ser exportado com o WSExport.';
